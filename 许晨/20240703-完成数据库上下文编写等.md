### Startup模式

```
using Admin2024.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Admin2024.Api;

public class Startup
{
    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    public void Configure(IApplicationBuilder app)
    {
        app.UseRouting();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();

        services.AddDbContext<Admin2024DbContext>(opt =>
        {
            opt.UseNpgsql(_configuration.GetConnectionString("pg"));
        });
    }
}


```

### 完成数据库上下文编写

```
using Microsoft.EntityFrameworkCore;
using Admin2024.Domain;
using Admin2024.Domain.Entity.System;

namespace Admin2024.EntityFrameworkCore;

public class Admin2024DbContext : DbContext
{
    public Admin2024DbContext(DbContextOptions<Admin2024DbContext> options) : base(options)
    {

    }

    public DbSet<AppUser> AppUser { get; set; }
    public DbSet<AppRole> AppRole { get; set; }
    public DbSet<AppUserRole> AppUserRole { get; set; }
    public DbSet<AppPermission> AppPermission { get; set; }
    public DbSet<AppRolePermission> AppRolePermission { get; set; }
    public DbSet<AppResource> AppResource { get; set; }
    public DbSet<AppOperation> AppOperation { get; set; }
}


```