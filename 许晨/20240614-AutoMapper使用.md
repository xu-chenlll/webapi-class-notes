## AutoMapper使用
第一步：安装，dotnet add package AutoMapper
第二步：配置，定义profile类型配置类
第三步：注册AutoMapper服务到容器
第四步：使用，在控制器或其它接口服务中，通过构造函数注入



## 思维导图如下：

@startmindmap
* AutoMapper使用
    * 第一步
        * 安装，dotnet add package AutoMapper
    * 第二步
        * 配置，定义profile类型配置类
    *第三步
        * 注册AutoMapper服务到容器
    *第四步
        * 使用，在控制器或其它接口服务中，通过构造函数注入

@endmindmap


## 关于AutoMapper

AutoMapper是一个基于命名约定的对象->对象映射工具
- 只要2个对象的属性具有相同名字（或者符合它规定的命名约定），AutoMApper就可以替我们自动在2个对象间进行属性值的映射。如果有不符合约定的映射，或者需要自定义映射行为，就需要我们事先告诉AutoMapper，所以在使用Map（src，dest）进行映射之前，必须使用CreateMap（）进行配置

```

Mapper.CreateMap<Product, ProductDto>(); // 配置
Product entity = Reop.FindProduct(id); // 从数据库中取得实体
Assert.AreEqual("挖掘机", entity.ProductName);
ProductDto productDto = Mapper.Map(entity); // 使用AutoMapper自动映射
Assert.AreEqual("挖掘机", productDto.ProductName);

```
AutoMapper就是这样一个只有2个常用函数的简单方便的工具。