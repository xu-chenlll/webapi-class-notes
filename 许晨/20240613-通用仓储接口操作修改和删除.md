## 修改和删除实际操作的替换，替换为通用仓储接口

### 修改
```
  [HttpPut("{bookId}")]
    public IActionResult Put(Guid authorId, Guid bookId, BookUpdateDto bookUpdateDto)
    {
        var author = _authorRep.GetById(authorId);
        if (author == null)
        {
            return NotFound();
        }
        var book = _bookRep.GetById(bookId);
        if (book == null)
        {
            return NotFound();
        }
        book.BookName = bookUpdateDto.BookName;
        book.Publisher = bookUpdateDto.Publisher;
        book.Price = bookUpdateDto.Price;
        var result = _bookRep.Update(book);
        return Ok(result);
    }


```

### 删除

```
   [HttpDelete("{bookId}")]
    public IActionResult Delete(Guid authorId, Guid bookId)
    {
        var author = _authorRep.GetById(authorId);
        if (author == null)
        {
            return NotFound();
        }
        var result = _bookRep.Delete(bookId);
        return Ok(result);
    }

```