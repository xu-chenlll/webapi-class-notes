##  搭建前端管理界面

main.js
```
import { createApp } from 'vue'
import App from './App.vue'
// 以下完整引入antdv
import antdv from 'ant-design-vue'
import 'ant-design-vue/dist/reset.css'

import { createRouter, createWebHistory } from 'vue-router'

let router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: () => import('./views/test.vue')
        },
        {
            path: '/about',
            component: () => import('./views/about.vue')
        }
    ]
})

let app = createApp(App);

app.use(router).use(antdv).mount('#app')

```